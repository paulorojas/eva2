<%-- 
    Document   : lista
    Created on : 20-abr-2020, 19:01:08
    Author     : LiL WOLF
--%>

<%@page import="root.model.entities.Cliente"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Cliente> clientes=(List<Cliente>)request.getAttribute("clientes");
    Iterator<Cliente> itClientes=clientes.iterator();
%>    


<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Clientes</title>
        <link rel="stylesheet" href="listastyle.css">
    </head>
    <body>
        
        <form  name="form" action="controllerListado" method="POST">
        
        <h1>Lista Clientes</h1>
        
        <div id="table">    
        <table class="table table-dark">
    <thead>
        <tr>
         <th scope="col">Nombre Usuario</th>
         <th scope="col">Nombre</th>
         <th scope="col">Apellido Paterno</th>
         <th scope="col">Apellido Materno</th>
         <th scope="col">Direccion</th>
         <th scope="col">Telefono</th>
        </tr>
    </thead>
      <tbody>
          <%while(itClientes.hasNext()){
              Cliente cli=itClientes.next();%>
          
        <tr>
         <td scope="row"><%=cli.getCliUsername()%></td>
         <td><%=cli.getCliNombre()%></td>
         <td><%=cli.getCliApellidoPaterno()%></td>
         <td><%=cli.getCliApellidoMaterno()%></td>
         <td><%=cli.getCliDireccion()%></td>
         <td><%=cli.getCliTelefono()%></td>
         
         <td><input type="radio"  name="seleccion" value="<%=cli.getCliUsername()%>"></td>
         
        </tr> 
     
       <%}%> 
    </tbody>
</table>
  
    <button type="submit" name="accion" value="crear" class="btn btn-success" style="font-size: 23px; text-align: center; margin-top: 20px; margin-left: 30%; height: 50px; width: 100px" >Crear</button>
    <button type="submit" name="accion" value="editar" class="btn btn-warning" style="font-size: 23px; text-align: center; margin-top: 20px; margin-left: 20px; height: 50px; width: 100px" >Editar</button>
    <button type="submit" name="accion" value="eliminar" class="btn btn-danger"style="font-size: 21px; text-align: center; margin-top: 20px; margin-left: 20px; height: 50px; width: 100px" >Eliminar</button>
   
    
    
    
    
   
    </div>
    
    
    
    
        </form>
    </body>
</html>
