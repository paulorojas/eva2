<%-- 
    Document   : editar
    Created on : 20-abr-2020, 19:53:33
    Author     : LiL WOLF
--%>

<%@page import="root.model.entities.Cliente"%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="crearstyle.css">
        <title>Crear</title>
    </head>
    <body>
        
       
        <form name="form" action="controllerCliente" method="POST">
           
            <h1>Crear</h1>
            
            <div id="form">
            <div class="form-row">
            <div class ="form-group col-md-6">
                <label for="username">Nombre Usuario</label>
                <input name="username" class="form-control" id="username" placeholder="Username" required>
            </div>
            
            <br>
           
             <div class ="form-group col-md-6">
                <label for="nombre">Nombre</label>
                <input name="nombre" class="form-control" id="nombre" placeholder="Nombre" required>
            </div>
            
            <br>
            
             <div class ="form-group col-md-6">
                <label for="apaterno">Apellido Paterno</label>
                <input name="apaterno" class="form-control" id="apaterno" placeholder="Paterno" required>
            </div>
            
            <br>
            
             <div class ="form-group col-md-6">
                <label for="amaterno">Apellido Materno</label>
                <input name="amaterno" class="form-control" id="amaterno" placeholder="Materno" required>
            </div>
            
            <br>
            
             <div class ="form-group col-md-6">
                <label for="direccion">Direccion</label>
                <input name="direccion" class="form-control" id="direccion" placeholder="Pasaje Estanque 123" required>
            </div>
            
            <br>
            
             <div class ="form-group col-md-6">
                <label for="telefono">Telefono</label>
                <input name="telefono" class="form-control" id="telefono" placeholder="Ej: 956871428" required>
            </div>
            
            <br>
            <button type="submit" name="accion" value="grabarCrear" class="btn btn-success" style="font-size: 23px; text-align: center; margin-top: 20px; margin-left: 35%; height: 50px; width: 100px" >Grabar</button>
            <button type="submit" class="btn btn-danger"style="font-size: 21px; text-align: center; margin-top: 20px; margin-left: 25px; height: 50px; width: 100px" >Salir</button>

          </div>
                
            </div>
        </form>
        
      
        
        
        
        
    </body>
</html>
