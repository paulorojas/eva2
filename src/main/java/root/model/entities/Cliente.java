/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LiL WOLF
 */
@Entity
@Table(name = "cliente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c"),
    @NamedQuery(name = "Cliente.findByCliUsername", query = "SELECT c FROM Cliente c WHERE c.cliUsername = :cliUsername"),
    @NamedQuery(name = "Cliente.findByCliNombre", query = "SELECT c FROM Cliente c WHERE c.cliNombre = :cliNombre"),
    @NamedQuery(name = "Cliente.findByCliApellidoPaterno", query = "SELECT c FROM Cliente c WHERE c.cliApellidoPaterno = :cliApellidoPaterno"),
    @NamedQuery(name = "Cliente.findByCliApellidoMaterno", query = "SELECT c FROM Cliente c WHERE c.cliApellidoMaterno = :cliApellidoMaterno"),
    @NamedQuery(name = "Cliente.findByCliDireccion", query = "SELECT c FROM Cliente c WHERE c.cliDireccion = :cliDireccion"),
    @NamedQuery(name = "Cliente.findByCliTelefono", query = "SELECT c FROM Cliente c WHERE c.cliTelefono = :cliTelefono")})
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "cli_username")
    private String cliUsername;
    @Size(max = 2147483647)
    @Column(name = "cli_nombre")
    private String cliNombre;
    @Size(max = 2147483647)
    @Column(name = "cli_apellido_paterno")
    private String cliApellidoPaterno;
    @Size(max = 2147483647)
    @Column(name = "cli_apellido_materno")
    private String cliApellidoMaterno;
    @Size(max = 2147483647)
    @Column(name = "cli_direccion")
    private String cliDireccion;
    @Column(name = "cli_telefono")
    private Integer cliTelefono;

    public Cliente() {
    }

    public Cliente(String cliUsername) {
        this.cliUsername = cliUsername;
    }

    public String getCliUsername() {
        return cliUsername;
    }

    public void setCliUsername(String cliUsername) {
        this.cliUsername = cliUsername;
    }

    public String getCliNombre() {
        return cliNombre;
    }

    public void setCliNombre(String cliNombre) {
        this.cliNombre = cliNombre;
    }

    public String getCliApellidoPaterno() {
        return cliApellidoPaterno;
    }

    public void setCliApellidoPaterno(String cliApellidoPaterno) {
        this.cliApellidoPaterno = cliApellidoPaterno;
    }

    public String getCliApellidoMaterno() {
        return cliApellidoMaterno;
    }

    public void setCliApellidoMaterno(String cliApellidoMaterno) {
        this.cliApellidoMaterno = cliApellidoMaterno;
    }

    public String getCliDireccion() {
        return cliDireccion;
    }

    public void setCliDireccion(String cliDireccion) {
        this.cliDireccion = cliDireccion;
    }

    public Integer getCliTelefono() {
        return cliTelefono;
    }

    public void setCliTelefono(Integer cliTelefono) {
        this.cliTelefono = cliTelefono;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cliUsername != null ? cliUsername.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        if ((this.cliUsername == null && other.cliUsername != null) || (this.cliUsername != null && !this.cliUsername.equals(other.cliUsername))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.model.entities.Cliente[ cliUsername=" + cliUsername + " ]";
    }

    public void setCliTelefono(String telefono) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
